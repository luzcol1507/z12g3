package luzcol.bankir;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.auth.FirebaseAuth;

public class MapaLocActivity extends AppCompatActivity {

    private ConstraintLayout constraintLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mapa_loc);
        constraintLayout = findViewById(R.id.idLayout);
    }
    //Menu hamburguesa
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.iteminicio:
                //Hacer algo ...
                setContentView(R.layout.activity_inicio);
                return true;
            case R.id.itemcerrar:
                //Hacer algo ...
                setContentView(R.layout.activity_login);
                FirebaseAuth.getInstance().signOut();
                Intent intent = new Intent(MapaLocActivity.this, Login.class);
                startActivity(intent);
                finish();
                break;
            case R.id.itemBuscar:
                Toast.makeText(this, "Clic en buscar", Toast.LENGTH_LONG).show();
                break;
        }
        return super.onOptionsItemSelected(item);
        }
}