package luzcol.bankir;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;

import java.util.LinkedList;

import luzcol.bankir.adapter.AdapterBanco;
import luzcol.bankir.model.Bcos;

public class LogoBancosActivity extends AppCompatActivity {
    private RecyclerView recyclerBanco;
    private AdapterBanco adapterBanco;
    private LinkedList<Bcos> bcosLinkedList = new LinkedList<Bcos>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_logo_bancos);
        recyclerBanco = findViewById(R.id.RecyclerBanco);

        bcosLinkedList.add(new Bcos(R.drawable.logobcocolombia, "BancoColombia","Carrera 10 # 27-17","(+57) 601343 0000","8:00-16:30","si"));
        bcosLinkedList.add(new Bcos(R.drawable.logobcocolombia,"Banco Itau","Carrera 10 # 27-17","(+57) 601343 0000","8:00-16:30","si"));
        bcosLinkedList.add(new Bcos(R.drawable.logobcobogota, "Banco de Bogotá","Carrera 10 # 27-17","(+57) 601343 0000","8:00-16:30","si"));
        bcosLinkedList.add(new Bcos(R.drawable.logocajasocial, "Banco Caja Social","Carrera 10 # 27-17","(+57) 601343 0000","8:00-16:30","si"));
        bcosLinkedList.add(new Bcos(R.drawable.logodavivienda, "Banco Davivienda","Carrera 10 # 27-17","(+57) 601343 0000","8:00-16:30","si"));
        bcosLinkedList.add(new Bcos(R.drawable.bbvalogo,"Banco: BBVA","Carrera 10 # 27-17","(+57) 601343 0000","8:00-16:30","si"));

        recyclerBanco.setLayoutManager(new LinearLayoutManager( this));
        adapterBanco = new AdapterBanco(bcosLinkedList);
        recyclerBanco.setAdapter(adapterBanco);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.iteminicio:
                //Hacer algo ...
                setContentView(R.layout.activity_logo_bancos);
                return true;
            case R.id.itemcerrar:
                //Hacer algo ...
                setContentView(R.layout.activity_login);
                FirebaseAuth.getInstance().signOut();
                Intent intent = new Intent(LogoBancosActivity.this,Login.class);
                startActivity(intent);
                finish();
                break;
            case R.id.itemBuscar:
                Toast.makeText(this, "Clic en buscar", Toast.LENGTH_LONG).show();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
