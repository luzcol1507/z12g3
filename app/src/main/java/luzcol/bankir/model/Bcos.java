package luzcol.bankir.model;

import android.content.DialogInterface;
import android.content.Intent;

import luzcol.bankir.BancoActivity;
import luzcol.bankir.InicioActivity;
import luzcol.bankir.LogoBancosActivity;

public class Bcos {
    private int imagen;
    private String nombre;
    private String direccion;
    private String telefono;
    private String horario;
    private String disponible;

    public Bcos() {
    }

    public Bcos(int imagen, String nombre, String direccion, String telefono, String horario, String disponible) {
        this.imagen = imagen;
        this.nombre = nombre;
        this.direccion = direccion;
        this.telefono = telefono;
        this.horario = horario;
        this.disponible = disponible;
    }

    public int getImagen() {
        return imagen;
    }

    public void setImagen(int imagen) {
        this.imagen = imagen;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getHorario() {
        return horario;
    }

    public void setHorario(String horario) {
        this.horario = horario;
    }

    public String getDisponible() {
        return disponible;
    }

    public void setDisponible(String disponible) {
        this.disponible = disponible;
    }

}
