package luzcol.bankir;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;


import java.util.Calendar;

import luzcol.bankir.model.Usuario;


public class RegistrarActivity extends AppCompatActivity {

    private EditText edNombreRegistro,edTelefonoRegistro,edDireccionRegistro,edCumpleañosRegistro,edCorreoRegistro,edContraseñaRegistro;
    private FirebaseAuth mAuth;
    private DatabaseReference mDatabase;
    FirebaseDatabase database = FirebaseDatabase.getInstance();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registrar);

        edContraseñaRegistro = findViewById(R.id.edContraseñaRegistro);
        edCorreoRegistro = findViewById(R.id.edCorreoRegistro);
        edNombreRegistro = findViewById(R.id.edNombreRegistro);
        edTelefonoRegistro = findViewById(R.id.edTelefonoRegistro);
        edDireccionRegistro = findViewById(R.id.edDireccionRegistro);
        edCumpleañosRegistro = findViewById(R.id.edCumpleañosRegistro);
        edCumpleañosRegistro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Calendar calendar = Calendar.getInstance();
                int year = calendar.get(Calendar.YEAR);
                int month = calendar.get(Calendar.MONTH);
                int dayOfMonth = calendar.get(Calendar.DAY_OF_MONTH);
                DatePickerDialog dialog = new DatePickerDialog(RegistrarActivity.this, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker datePicker, int year, int month, int dayOfMont) {
                        edCumpleañosRegistro.setText(year+"-"+(month+1)+"-"+dayOfMont);
                    }
                },year,month,dayOfMonth);
                dialog.show();
            }
        });
        mAuth = FirebaseAuth.getInstance();
        mDatabase = FirebaseDatabase.getInstance().getReference();


    }
    public void registrarUsuario(View view){
        String correo = edCorreoRegistro.getText().toString();
        String contraseña = edContraseñaRegistro.getText().toString();
        String nombre = edNombreRegistro.getText().toString();
        String telefono = edTelefonoRegistro.getText().toString();
        String direccion = edDireccionRegistro.getText().toString();
        String fechaCumpleaños = edCumpleañosRegistro.getText().toString();

        mAuth.createUserWithEmailAndPassword(correo, contraseña)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            FirebaseUser user = mAuth.getCurrentUser();
                            String id = user.getUid();
                            Usuario usuario = new Usuario(nombre,telefono,direccion,fechaCumpleaños,correo);
                            mDatabase.child("usuarios").child(id).setValue(usuario);
                            Intent intent = new Intent(RegistrarActivity.this,BancoActivity.class);
                            startActivity(intent);
                            finish();
                        } else {
                            Log.w("failed create", "createUserWithEmail:failure", task.getException());
                            Toast.makeText(RegistrarActivity.this, "Autenticación fallida.",
                                    Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }

}