package luzcol.bankir;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.PermissionChecker;
import androidx.viewpager.widget.ViewPager;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.view.Menu;

import android.os.Bundle;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.Toast;

import android.view.View;
import android.widget.AdapterView;
import android.widget.Toolbar;

import com.google.android.material.tabs.TabLayout;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import luzcol.bankir.controlador.PageController;
import luzcol.bankir.model.Bcos;
import luzcol.bankir.model.Usuario;


public class InicioActivity extends AppCompatActivity {

    private ImageView imageBanco ;
    private EditText edBanco, edNombre, edDireccion, edTelefono, edHorario, edDisponible ;
    /* private DatabaseReference mDatabase;
    FirebaseDatabase database = FirebaseDatabase.getInstance();*/

    private Spinner TBanco;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inicio);

        edNombre = findViewById(R.id.nombreBanco);
        imageBanco = findViewById(R.id.imageBanco);
        edDireccion= findViewById(R.id.direccionBanco);
        edTelefono= findViewById(R.id.telefonoBanco);
        edHorario=findViewById(R.id.horarioBanco);
        edDisponible=findViewById(R.id.disponibleBanco);

        String nameBanco = getIntent().getStringExtra("nameBanco");

        switch (nameBanco){
            case "Banco Caja Social":
                imageBanco.setImageResource(R.drawable.logocajasocial);
                break;
            case "Bancolombia":
                imageBanco.setImageResource(R.drawable.logobcocolombia);
                break;
            case "Banco de Bogota":
                imageBanco.setImageResource(R.drawable.logobcobogota);
                break;
            case "Banco Itau":
                imageBanco.setImageResource(R.drawable.logoitau);
                break;
            case "Davivienda":
                imageBanco.setImageResource(R.drawable.logodavivienda);
                break;
            case "Banco BBVA":
                imageBanco.setImageResource(R.drawable.bbvalogo);
                imageBanco.setBackground(getDrawable(R.color.base));
                break;
        }
        edNombre.setText(nameBanco);
        //mDatabase = FirebaseDatabase.getInstance().getReference();

    }

    public void mapa (View view) {
        Intent intent = new Intent(InicioActivity.this, MapaActivity.class);
        startActivity(intent);
        finish();
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_submenu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.itemcerrar:
                //Hacer algo ...
                setContentView(R.layout.activity_login);
                FirebaseAuth.getInstance().signOut();
                Intent intent = new Intent(InicioActivity.this, Login.class);
                startActivity(intent);
                finish();
                break;
            case R.id.itemBuscar:
                Toast.makeText(this, "Buscar", Toast.LENGTH_LONG).show();
                break;
        }
                return super.onOptionsItemSelected(item);
        }
     /*public void registrarInfoBanco(View view){
        String nombre= edNombre.getText().toString();
        String direccion=edDireccion.getText().toString();
        String horario=edHorario.getText().toString();
        String disponible=edDisponible.getText().toString();
            if (nombre.isEmpty()||direccion.isEmpty()||horario.isEmpty()||disponible.isEmpty()){
                Toast.makeText(InicioActivity.this, "Complete todos los campos", Toast.LENGTH_LONG).show();
            }
            else {
                Bcos banco = new Bcos();
            }
        }*/
            /*
    Usuario usuario = new Usuario(nombre,telefono,direccion,fechaCumpleaños,correo);
    mDatabase.child("usuarios").child(id).setValue(usuario);
    Intent intent = new Intent(RegistrarActivity.this,BancoActivity.class);
    startActivity(intent);
    finish();
    edNombre.isEmpty*/


}


