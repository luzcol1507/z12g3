package luzcol.bankir.fragmento;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.Spinner;

import luzcol.bankir.InicioActivity;
import luzcol.bankir.R;
import com.google.android.gms.location.*;


public class IncioFragment extends Fragment implements View.OnClickListener{

    private Spinner spinnerTbanco;
    private ImageButton btCajaSocial,btBcoColombia,btBcoBogota,btItau,btDavivienda,btBbva;
    private FusedLocationProviderClient fusedLocationProviderClient;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_incio, container, false);
        spinnerTbanco = view.findViewById(R.id.spinnerTbanco);
        btCajaSocial = view.findViewById(R.id.btCajaSocial);
        btBcoColombia = view.findViewById(R.id.btBcoColombia);
        btBcoBogota = view.findViewById(R.id.btBcoBogota);
        btItau = view.findViewById(R.id.btItau);
        btDavivienda = view.findViewById(R.id.btDavivienda);
        btBbva = view.findViewById(R.id.btBbva);

        btCajaSocial.setOnClickListener(this::onClick);
        btBcoColombia.setOnClickListener(this::onClick);
        btBcoBogota.setOnClickListener(this::onClick);
        btItau.setOnClickListener(this::onClick);
        btDavivienda.setOnClickListener(this::onClick);
        btBbva.setOnClickListener(this::onClick);

        int idArrayTbanco = R.array.arraytbanco;
        int idLayoutSpinner = android.R.layout.simple_spinner_dropdown_item;
        ArrayAdapter adapter = ArrayAdapter.createFromResource(getContext(), idArrayTbanco, idLayoutSpinner);
        spinnerTbanco.setAdapter(adapter);
        return view;
    }



    @Override
    public void onClick(View view) {
        String banco = null;
        switch (view.getId()){
            case R.id.btCajaSocial:
                banco = "Banco Caja Social"; //child
                break;
            case R.id.btBcoColombia:
                banco = "Bancolombia";
                break;
            case R.id.btBcoBogota:
                banco = "Banco de Bogota";
                break;
            case R.id.btItau:
                banco = "Banco Itau";
                break;
            case R.id.btDavivienda:
                banco = "Davivienda";
                break;
            case R.id.btBbva:
                banco = "Banco BBVA";
                break;
        }
        continuar(banco);

    }

    private void continuar(String nameBanco) {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(getContext())
                .setTitle("Bankir App")
                .setMessage("Para Continuar")
                .setCancelable(false)

                .setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                    }
                })
                .setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        Intent ingresar = new Intent(getContext(), InicioActivity.class);
                        ingresar.putExtra("nameBanco",nameBanco);
                        startActivity(ingresar);
                    }
                });
        alertDialog.show();
    }
}