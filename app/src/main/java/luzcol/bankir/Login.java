package luzcol.bankir;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.PermissionChecker;


import android.Manifest;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import android.widget.EditText;

import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class Login extends AppCompatActivity {
    // private Button btCardview;
    private EditText edcontrasena,edcorreo;
    private FirebaseAuth mAuth;
    private Button btInicio;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        edcontrasena=findViewById(R.id.edcontrasena);
        edcorreo=findViewById(R.id.edcorreo);
        mAuth = FirebaseAuth.getInstance();
        btInicio = findViewById(R.id.btInicio);
    }

    public void registrar(View view){
        Intent intent = new Intent(Login.this,RegistrarActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void onStart() {
        super.onStart();
        FirebaseUser currentUser = mAuth.getCurrentUser();
        if (currentUser != null) {
            InfoLaunch();
        }
    }

     public void iniciarSesion(View view){
        String email = edcorreo.getText().toString();
        String password = edcontrasena.getText().toString();

         if(verificarCampos(email,"password")) {
             mAuth.signInWithEmailAndPassword(email, password)
                     .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                         @Override
                         public void onComplete(@NonNull Task<AuthResult> task) {
                             if (task.isSuccessful()) {
                                 // Sign in success, update UI with the signed-in user's information
                                 Log.d("Sucess", "signInWithEmail:success");
                                 FirebaseUser user = mAuth.getCurrentUser();
                                 Toast.makeText(Login.this, "Inició correctamente", Toast.LENGTH_LONG).show();
                                 InfoLaunch();
                                 //updateUI(user);
                             } else {
                                 // If sign in fails, display a message to the user.
                                 Log.w("Failed", "signInWithEmail:failure", task.getException());
                                 Toast.makeText(Login.this, "Authentication failed.",
                                         Toast.LENGTH_SHORT).show();
                                 //updateUI(null);
                             }
                         }
                     });
         }
    }


    public void olvidoContraseña(View view){
        String email = edcorreo.getText().toString();
        if(verificarCampos(email,"password")){
            mAuth.sendPasswordResetEmail(email).addOnCompleteListener(this, new OnCompleteListener<Void>() {
                @Override
                public void onComplete(@NonNull Task<Void> task) {
                    if (task.isSuccessful()) {
                        Toast.makeText(Login.this, "Se envio el correo", Toast.LENGTH_LONG).show();
                    } else {
                        Toast.makeText(Login.this, "No se envio el correo", Toast.LENGTH_LONG).show();
                        Log.e("ErrorSendEmail", task.getException().toString());
                    }
                }
            });
        }
    }

////esto cambié..
    public void InfoLaunch(){
        Intent intent =new Intent(Login.this,HomeActivity.class);
        startActivity(intent);
        finish();
    }

    private boolean verificarCampos(String email,String password){
        Toast.makeText(this, "Prueba"+email.isEmpty(), Toast.LENGTH_SHORT).show();
        if(email.isEmpty() && password.isEmpty()){
            edcorreo.setError("El correo es requerido");
            edcontrasena.setError("La contraseña es requerida");
            return false;
        }else if(email.isEmpty()){
            edcorreo.setError("El correo es requerido");
            return false;
        }else if(password.isEmpty()){
            edcontrasena.setError("La contraseña es requerida");
            return false;
        }else{
            return true;
        }
    }
}
