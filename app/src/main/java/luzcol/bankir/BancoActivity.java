package luzcol.bankir;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.auth.FirebaseAuth;

public class BancoActivity extends AppCompatActivity {
    private ConstraintLayout constraintLayout;
    private Spinner spinnerTbanco;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_banco);
        constraintLayout = findViewById(R.id.idLayout);
        spinnerTbanco = findViewById(R.id.spinnerTbanco);
        getIntent().getStringExtra("nombre");

        int idArrayTbanco = R.array.arraytbanco;
        int idLayoutSpinner = android.R.layout.simple_spinner_dropdown_item;
        ArrayAdapter adapter = ArrayAdapter.createFromResource(this, idArrayTbanco, idLayoutSpinner);
        spinnerTbanco.setAdapter(adapter);
    }
    public void dialogo(View view){
      AlertDialog.Builder dialoglist = new AlertDialog.Builder( this)
                .setTitle("Entidades Financieras")
                .setItems(R.array.financiero, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                    Snackbar.make(constraintLayout,String.valueOf(i), Snackbar.LENGTH_LONG).show();
                        switch (i){
                            case 0:
                                Intent ingresar = new Intent(BancoActivity.this, MapaActivity.class);
                                startActivity(ingresar);
                                finish();
                                break;
                            case 1:
                                Snackbar.make(constraintLayout, "Clic Cajero Automático", Snackbar.LENGTH_LONG).show();
                                break;
                            case 2:
                                Snackbar.make(constraintLayout, "Clic Corresponsal", Snackbar.LENGTH_LONG).show();
                                break;
                        }
                    }
                })
                ;
        dialoglist.show();
      }
    //Menu hamburguesa
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main,menu);
        return true;
    }

    /*public void registrar(View view){
        Intent intent = new Intent(Login.this,RegistrarActivity.class);
        startActivity(intent);
        finish();
    }*/

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.iteminicio:
                //Hacer algo ...
                setContentView(R.layout.activity_inicio);
                return true;
            case R.id.itemcerrar:
                //Hacer algo ...
                setContentView(R.layout.activity_login);
                FirebaseAuth.getInstance().signOut();
                Intent intent = new Intent(BancoActivity.this,Login.class);
                startActivity(intent);
                finish();
                break;
            case R.id.itemBuscar:
                Toast.makeText(this, "Buscar", Toast.LENGTH_LONG).show();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
    }