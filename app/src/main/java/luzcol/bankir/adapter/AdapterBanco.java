package luzcol.bankir.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import luzcol.bankir.BancoActivity;
import luzcol.bankir.R;
import luzcol.bankir.model.Bcos;

public class AdapterBanco extends RecyclerView.Adapter<AdapterBanco.BancosViewHolder> {

    private static List<Bcos> bancosList;

    public AdapterBanco(List<Bcos> bancosList) {
        this.bancosList = bancosList;
   }

    @NonNull
    @Override
    public AdapterBanco.BancosViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v= LayoutInflater.from(parent.getContext()).inflate(R.layout.cardview_bancos,parent, false);
        return new BancosViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull BancosViewHolder holder, int position) {
        holder.imagenBanco.setImageResource(bancosList.get(position).getImagen());
        holder.nombreBanco.setText(bancosList.get(position).getNombre());
        holder.direccionBanco.setText(bancosList.get(position).getDireccion());
        holder.telefonoBanco.setText(bancosList.get(position).getTelefono());
        holder.horarioBanco.setText(bancosList.get(position).getHorario());
        holder.disponibleBanco.setText(bancosList.get(position).getDisponible());
        holder.position=holder.getAdapterPosition();
    }

    @Override
    public int getItemCount() {
        return bancosList.size();
    }

    public static class BancosViewHolder extends RecyclerView.ViewHolder {
        private ImageView imagenBanco;
        private TextView nombreBanco, direccionBanco, telefonoBanco, horarioBanco, disponibleBanco;
        private int position;

        public BancosViewHolder(@NonNull View itemView) {
            super(itemView);
            imagenBanco = itemView.findViewById(R.id.imagenBanco);
            nombreBanco = itemView.findViewById(R.id.nombreBanco);
            direccionBanco = itemView.findViewById(R.id.direccionBanco);
            telefonoBanco = itemView.findViewById(R.id.telefonoBanco);
            horarioBanco = itemView.findViewById(R.id.horarioBanco);
            disponibleBanco = itemView.findViewById(R.id.direccionBanco);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Context context = view.getContext();
                    Intent intent = new Intent(context, BancoActivity.class);
                    intent.putExtra("nombre",bancosList.get(position).getNombre());
                    intent.putExtra("direccion",bancosList.get(position).getDireccion());
                    intent.putExtra("telefono",bancosList.get(position).getTelefono());
                    intent.putExtra("horario",bancosList.get(position).getHorario());
                    intent.putExtra("disponible",bancosList.get(position).getDisponible());
                    intent.putExtra( "imagen",bancosList.get(position).getDisponible());
                    context.startActivity(intent);
                }
            });
        }
    }
}
