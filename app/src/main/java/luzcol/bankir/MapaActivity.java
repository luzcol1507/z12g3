package luzcol.bankir;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.PermissionChecker;


import com.google.android.gms.location.*;
import com.google.android.gms.maps.*;

import com.google.android.gms.tasks.OnSuccessListener;

import android.Manifest;
import android.annotation.SuppressLint;
import android.location.Location;
import android.os.Bundle;
import android.widget.Toast;



public class MapaActivity extends AppCompatActivity
        implements
        OnMapReadyCallback
{
 private FusedLocationProviderClient fusedLocationProviderClient;

    @Override
    protected void onCreate(Bundle savedInstanceState)  {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mapa);

        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this);
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.maps);
        mapFragment.getMapAsync(this);
    }

    // revisar como unir cada banco
    public void home (String nameBanco) {
        Intent ingresar = new Intent(MapaActivity.this, InicioActivity.class);
        ingresar.putExtra("nameBanco",nameBanco);
        startActivity(ingresar);
//        startActivity(intent);
        finish();
    }
    private boolean verificarPermisos(){
        Boolean permisoFine = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PermissionChecker.PERMISSION_GRANTED;
        Boolean permisoCoarse = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) == PermissionChecker.PERMISSION_GRANTED;

        if(permisoCoarse && permisoFine){
            return true;
        }else{
            ActivityCompat.requestPermissions(this,new String[] {Manifest.permission.ACCESS_FINE_LOCATION,Manifest.permission.ACCESS_COARSE_LOCATION},0);
            return false;
        }
    }

    @SuppressLint("MissingPermission")
    @Override
    public void onMapReady(@NonNull GoogleMap googleMap) {
        if(verificarPermisos()){
            fusedLocationProviderClient.getLastLocation().addOnSuccessListener(new OnSuccessListener<Location>() {
                @Override
                public void onSuccess(Location location) {
                    if(location!=null){
                        double longitud = location.getLongitude();
                        double latitud = location.getLatitude();
                        double altitud = location.getAltitude();
                        LatLng miCordenadas = new LatLng(latitud,longitud);
                        googleMap.addMarker(new MarkerOptions().position(miCordenadas).title("Aqui estoy yo"));
                        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(miCordenadas,14));
                    }else{
                        Toast.makeText(MapaActivity.this, "Error al obtener la ubicacion", Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }
    }
}