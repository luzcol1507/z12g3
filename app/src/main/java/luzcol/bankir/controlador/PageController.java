package luzcol.bankir.controlador;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import luzcol.bankir.fragmento.FavoritoFragment;
import luzcol.bankir.fragmento.IncioFragment;

public class PageController extends FragmentPagerAdapter {

    private int numDeItemTabs;

    public PageController(@NonNull FragmentManager fm, int numDeItemTabs) {
        super(fm, numDeItemTabs);
        this.numDeItemTabs = numDeItemTabs;
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {
        switch (position){
            case 0: return new IncioFragment();
            case 1: return new FavoritoFragment();
            default: return null;
        }
    }

    @Override
    public int getCount() {
        return numDeItemTabs;
    }
}
